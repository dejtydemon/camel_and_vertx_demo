package com.momo.demo.http;


import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "camelConfig")
public class OutputmapConfig {
	List<OutMapping> outboundMapping=new ArrayList<OutputmapConfig.OutMapping>();
	
	public List<OutMapping> getOutboundMapping() {
		return outboundMapping;
	}

	@XmlElement
	public void setOutboundMapping(List<OutMapping> outboundMapping) {
		this.outboundMapping = outboundMapping;
	}
	
	@Override
	public String toString() {
		return "OutputmapConfig [outboundMapping=" + outboundMapping + "]";
	}

	@XmlRootElement(name= "outboundMapping")
	final static class OutMapping{
		private String id;
		private String fromCame;
		private String toCamel;

		public String getId() {
			return id;
		}
		
		@XmlAttribute
		public void setId(String id) {
			this.id = id;
		}
		public String getFromCame() {
			return fromCame;
		}
		
		@XmlElement
		public void setFromCame(String fromCame) {
			this.fromCame = fromCame;
		}
		public String getToCamel() {
			return toCamel;
		}
		
		@XmlElement
		public void setToCamel(String toCamel) {
			this.toCamel = toCamel;
		}

		@Override
		public String toString() {
			return "OutMapping [id=" + id + ", fromCame=" + fromCame + ", toCamel=" + toCamel + "]";
		}
		
		
		
	}
}
