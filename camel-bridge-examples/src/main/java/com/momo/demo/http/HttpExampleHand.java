package com.momo.demo.http;

import io.vertx.camel.CamelBridge;
import io.vertx.camel.CamelBridgeOptions;
import io.vertx.camel.InboundMapping;
import io.vertx.camel.OutboundMapping;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;

import org.apache.camel.CamelContext;
import org.apache.camel.CamelContextAware;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static io.vertx.camel.OutboundMapping.fromVertx;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * @author <a href="http://escoffier.me">Clement Escoffier</a>
 */
public class HttpExampleHand extends AbstractVerticle {

	 public static void main(String[] args) throws JAXBException {
		    Vertx vertx = Vertx.vertx();
		    DeploymentOptions option=new DeploymentOptions();
		    option.setWorker(true);
		    vertx.deployVerticle(HttpExampleHand.class.getName(), option);
		 
		  }
		  
	      //path: src/main/resources/META-INF/spring
		  @Override
		  public void start(Future<Void> fut) throws Exception {

//			  ApplicationContext app;
//				 try {
//					 app = new ClassPathXmlApplicationContext("META-INF/spring/camelContext.xml");
//					 System.out.println("RMI===> "+app.CLASSPATH_ALL_URL_PREFIX);
//				} catch (Exception e) {
//					System.out.println("err_create path: "+e);
//					throw e;
//				}
			   
			    CamelContext camel = new DefaultCamelContext();
			    camel.addRouteDefinitions(camel.loadRoutesDefinition(new FileInputStream("camelContext.xml")).getRoutes());
				CamelBridgeImplDemo bridge = new CamelBridgeImplDemo(vertx, new CamelBridgeOptions(camel)
					        .addOutboundMapping(OutboundMapping.fromVertx("direct:start").toCamel("direct:start"))
					        .addOutboundMapping(OutboundMapping.fromVertx("direct:bwaco_checkinfo2").toCamel("direct:bwaco_checkinfo2"))
//					        .addOutboundMapping(OutboundMapping.fromVertx("camel-route-fail").toCamel("direct:startFail")) 
					        );
//				CamelUpdateCheck.getIns().onStart(camel,bridge);
				
				camel.start();
				
				bridge.start();
			  
			// Create a router object.
			Router router = Router.router(vertx);
			
			// router request ok.
		router.route("/a").handler(routingContext -> {
			HttpServerRequest req = routingContext.request();

			System.out.println("IP REQUEST= " + getClientIpAddr(req));

			System.out.println("METHOD IS NOT VALID: " + req.rawMethod());
//			req.response().end("<h1>REQUEST INVALID</h1>");

			vertx.eventBus().<String>send("direct:bwaco_checkinfo2", "aaaaaa", reply -> {
				System.out.println("==> response: " + reply.result().body());
				if (reply.failed()) {
					req.response().setStatusCode(400).end(reply.cause().getMessage());
				} else {
					req.response().setStatusCode(0).end(reply.result().body());
				}
				req.response().close();
				System.out.println("======================================\n\n");
			});

		});
						
		
		router.route("/aa").handler(routingContext -> {
			HttpServerRequest req = routingContext.request();

			System.out.println("IP REQUEST= " + getClientIpAddr(req));

			System.out.println("METHOD IS NOT VALID: " + req.rawMethod());
//			req.response().end("<h1>REQUEST INVALID</h1>");

			vertx.eventBus().<String>send("direct:start", "bbbbbb", reply -> {
				System.out.println("==> response: " + reply.result().body());
				if (reply.failed()) {
					req.response().setStatusCode(400).end(reply.cause().getMessage());
				} else {
					req.response().setStatusCode(0).end(reply.result().body());
				}
				req.response().close();
				System.out.println("======================================\n\n");
			});

		});
						
					

			// router default
			router.route("/*").handler(routingContext -> {
				HttpServerRequest req = routingContext.request();
				System.out.println("IP REQUEST= "+ getClientIpAddr(req));
				System.out.println("request fail: " + req.getHeader("Accept"));
				
				
				if (!"POST".equals(req.rawMethod())) {
					vertx.eventBus().<String>send("camel-route-fail", "nothing", reply -> {
				    	System.out.println("==> response: "+reply.result().body());
				      if (reply.failed()) {
				    	  req.response().setStatusCode(400).end(reply.cause().getMessage());
				      } else {
				    	  req.response().setStatusCode(0).end(reply.result().body());
				      }
				      req.response().close();
					  System.out.println("======================================\n\n");
				    });
					
				}else {
				//code test
				vertx.eventBus().<String>send("camel-route-fail", "nothing", reply -> {
			    	System.out.println("==> response: "+reply.result().body());
			      if (reply.failed()) {
			    	  req.response().setStatusCode(400).end(reply.cause().getMessage());
			      } else {
			    	  req.response().setStatusCode(0).end(reply.result().body());
			      }
			      req.response().close();
				  System.out.println("======================================\n\n");
			    });
				}
				//end code test
				
//				HttpServerResponse response = routingContext.response();
//				response.putHeader("content-type", "text/html").end("<h1>404 PAGE NOT FOUND</h1>");
			});

			// Create the HTTP server and pass the "accept" method to the request handler.
			vertx.createHttpServer().requestHandler(router::accept).listen(
					// Retrieve the port from the configuration,
					// default to 8080.
					config().getInteger("http.port", 8088), result -> {
						if (result.succeeded()) {
							fut.complete();
						} else {
							fut.fail(result.cause());
						}
					});

		}
		  
		  
		  
		  
		  public static String getClientIpAddr(HttpServerRequest request) {  
		        String ip = request.getHeader("X-Forwarded-For");  
		        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
		            ip = request.getHeader("Proxy-Client-IP");  
		        }  
		        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
		            ip = request.getHeader("WL-Proxy-Client-IP");  
		        }  
		        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
		            ip = request.getHeader("HTTP_CLIENT_IP");  
		        }  
		        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
		            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
		        }  
		        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
		            ip = request.remoteAddress().host();  
		        }  
		        return ip;  
		    }  
}
