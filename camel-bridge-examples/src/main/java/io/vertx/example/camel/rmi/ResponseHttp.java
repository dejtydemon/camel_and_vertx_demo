package io.vertx.example.camel.rmi;

import java.rmi.RemoteException;

public class ResponseHttp implements HelloService{

	@Override
	public String hello(String name) throws RemoteException {
		return "http response: "+name;
	}

}
