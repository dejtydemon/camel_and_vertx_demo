<#assign beer_json_string_with_reference = "${body}">
<#assign beer_map_with_ref = beer_json_string_with_reference?eval>

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
   <soapenv:Header/>
   <soapenv:Body>
      <tem:GetCongNoChuaThuInfo>
         <!--Optional:-->
         <tem:user>TESTBANK</tem:user>
         <!--Optional:-->
         <tem:pass>a</tem:pass>
         <!--Optional:-->
         <tem:sodb>${beer_map_with_ref.sodb}</tem:sodb>
      </tem:GetCongNoChuaThuInfo>
   </soapenv:Body>
</soapenv:Envelope>