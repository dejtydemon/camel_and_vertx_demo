package com.momo.demo.freemaker;

import java.io.File;
import java.io.FileInputStream;
import javax.xml.bind.JAXBException;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.PropertyConfigurator;

import com.momo.demo.http.CamelBridgeImplDemo;

import io.vertx.camel.CamelBridgeOptions;
import io.vertx.camel.OutboundMapping;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;

public class BridgeCamelDemo extends AbstractVerticle{
	public static void main(String[] args) throws JAXBException {
		PropertyConfigurator.configure("/Users/AnhLe/git/camel_and_vertx_demo/camel-bridge-examples/camelConf/log4j.properties");
		File file=new File("/Users/AnhLe/git/camel_and_vertx_demo/camel-bridge-examples/camelConf/camelContext.xml");
		if(!file.exists()) {
			System.out.println("file not exist!!!");
			return;
		}else {
			System.out.println("file is exist!!!");
		}
		Vertx vertx = Vertx.vertx();
		DeploymentOptions option = new DeploymentOptions();
		option.setWorker(true);
		vertx.deployVerticle(BridgeCamelDemo.class.getName(), option);

	}

	@Override
	public void start(Future<Void> fut) throws Exception {

		CamelContext camel = new DefaultCamelContext();
		camel.addRouteDefinitions(camel.loadRoutesDefinition(new FileInputStream("/Users/AnhLe/git/camel_and_vertx_demo/camel-bridge-examples/camelConf/camelContext.xml")).getRoutes());

		CamelBridgeImplDemo bridge = new CamelBridgeImplDemo(vertx, new CamelBridgeOptions(camel)
				.addOutboundMapping(OutboundMapping.fromVertx("direct:start").toCamel("direct:start")));
		camel.start();

		bridge.start();

		// Create a router object.
		Router router = Router.router(vertx);

		// router default
		router.route("/*").handler(routingContext -> {
			HttpServerRequest req = routingContext.request();
			System.out.println("IP REQUEST= " + getClientIpAddr(req));
			req.bodyHandler(body->{
				try {
					String rawRequest = new String(body.getBytes());
					System.out.println("====================Start==================\n\n");
					System.out.println("==> Request: "+rawRequest+"\n\n");
					JsonObject json=new JsonObject(rawRequest);
					vertx.eventBus().<String>send("direct:start",json, reply -> {
						try {
							System.out.println("==> response: " + reply.result().body());
							if (reply.failed()) {
								req.response().setStatusCode(400).end(reply.cause().getMessage());
							} else {
								req.response().setStatusCode(0).end(reply.result().body());
							}
							req.response().close();
							System.out.println("===================End=====================\n\n");
						} catch (Exception e) {
							System.out.println("==> response: err handle!!!"+e);
							System.out.println("===================End=====================\n\n");
							req.response().setStatusCode(400).end("SYSTEM BUSY");
							req.response().close();
						}
						
					});
				} catch (Exception e) {
					req.response().setStatusCode(400).end("SYSTEM BUSY");
					req.response().close();
					System.out.println("==> response: err handle!!!"+e);
					System.out.println("===================End=====================\n\n");
				}
				
			});
		});

		// Create the HTTP server and pass the "accept" method to the request handler.
		vertx.createHttpServer().requestHandler(router::accept).listen(
				// Retrieve the port from the configuration,
				// default to 8080.
				config().getInteger("http.port", 8088), result -> {
					if (result.succeeded()) {
						fut.complete();
					} else {
						fut.fail(result.cause());
					}
				});

		}
		  
		  
		  
		  
		  public static String getClientIpAddr(HttpServerRequest request) {  
		        String ip = request.getHeader("X-Forwarded-For");  
		        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
		            ip = request.getHeader("Proxy-Client-IP");  
		        }  
		        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
		            ip = request.getHeader("WL-Proxy-Client-IP");  
		        }  
		        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
		            ip = request.getHeader("HTTP_CLIENT_IP");  
		        }  
		        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
		            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
		        }  
		        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
		            ip = request.remoteAddress().host();  
		        }  
		        return ip;  
		    }  
	
	
	
}
