package com.momo.demo.http;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.camel.CamelContext;
import org.apache.camel.model.RoutesDefinition;

import io.vertx.camel.CamelBridgeOptions;
import io.vertx.camel.OutboundMapping;

public class CamelUpdateCheck implements Runnable{
	private static CamelUpdateCheck instance;
	private boolean isRun=true;
	private Thread thread;
	private CamelContext camel;
	private CamelBridgeImplDemo camelBridge;
	private long lastEdit=0;
	
	public static synchronized CamelUpdateCheck getIns() {
		if(instance==null) {
			instance=new CamelUpdateCheck();
		}
		return instance;
	}
	
	private CamelUpdateCheck() {
	};
	
	public void onStart(CamelContext camel,CamelBridgeImplDemo camelBridge) {
		thread=new Thread(instance);
		thread.start();
		this.camel=camel;
		this.camelBridge=camelBridge;
	}
	
	public void onStop() {
		this.isRun=false;
		if(thread!=null) {
			thread.stop();
		}
	}

	@Override
	public void run() {
		while(isRun) {
			try {
				
				System.out.println("run");
				Thread.sleep(30000);
				System.out.println("start add camel context");
				InputStream is = new FileInputStream("src/main/resources/META-INF/spring/camelContext_2.xml");
				RoutesDefinition routes = this.camel.loadRoutesDefinition(is);
				this.camel.addRouteDefinitions(routes.getRoutes());
				routes.getRoutes().forEach(s->{
					System.out.println("id root= "+s.getId());
					System.out.println("id root= "+s.getId());
					
				});
				
				System.out.println("end update camel context");
				
				
				Thread.sleep(30000);
				System.out.println("start load bridgel camel context");
				loadBrigeXml();
				System.out.println("end load bridgel camel context");
				Thread.sleep(2000000);
			} catch (InterruptedException e) {
				
			} catch (Exception e) {
				System.err.println("err!!"+e);
			}
		}
		
		System.out.println("stop");
	}

	private void loadBrigeXml() throws JAXBException {
		OutputmapConfig outConf;
		File file = new File("src/main/resources/META-INF/spring/CamelBridge.xml");
		if (lastEdit == file.lastModified()) {
			return;
		}
		lastEdit = file.lastModified();
		JAXBContext jaxbContext = JAXBContext.newInstance(OutputmapConfig.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		outConf = (OutputmapConfig) jaxbUnmarshaller.unmarshal(file);
		System.out.println("==> out conf: " + outConf.toString());
		
		CamelBridgeOptions option=new CamelBridgeOptions(this.camel);
		outConf.getOutboundMapping().forEach(conf->{
			option.addOutboundMapping(OutboundMapping.fromVertx(conf.getFromCame()).toCamel(conf.getToCamel()));
		});
		this.camelBridge.addBrigeOutEx(option);
		this.camelBridge.start();
	}

}
