package io.vertx.example.camel.rmi;

import javax.print.DocFlavor.STRING;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;

public class MainCamelDemo {
public static void main(String[] args) {
	CamelContext camelContext = new DefaultCamelContext();
    ProducerTemplate template = camelContext.createProducerTemplate();

//    Exchange exchange = template.send("http://localhost:8080", new Processor() {
//        public void process(Exchange exchange) throws Exception {
//            exchange.getIn().setHeader(Exchange.HTTP_QUERY, "/aa");
//        }
//    });
//
//    Message out = exchange.getOut();
//    System.out.println("Response from http template is "+exchange.getOut().getBody());
////    System.out.println("status header is "+out.getHeader(Exchange.HTTP_RESPONSE_CODE));
    
		Exchange exchange = template.send("http://localhost:8080/aa", new Processor() {
			public void process(Exchange exchange) throws Exception {
				exchange.getIn().setHeader(Exchange.HTTP_QUERY, "hl=en&q=activemq");
				exchange.getIn().setHeader(Exchange.HTTP_METHOD, "POST");
				exchange.getIn().setBody("{\n" + 
						"   \"appCode\": \"EU 2.0.24\",\n" + 
						"   \"appVer\": 2182,\n" + 
						"   \"channel\": \"APP\",\n" + 
						"   \"cmdId\": \"190556351230455\",\n" + 
						"   \"deviceOS\": \"Android\",\n" + 
						"   \"errorCode\": 999\n" + 
						"   \n" + 
						"}");
			}
		});
		Message out = exchange.getOut();
		int responseCode = out.getHeader(Exchange.HTTP_RESPONSE_CODE, Integer.class);
		System.out.println("===> " + responseCode);
		System.out.println("Response from http template is: "+out.getBody(String.class));
}
}
