package com.momo.demo.freemaker;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import io.vertx.core.json.JsonObject;

public class HandRoute {
	private static final Logger logger=Logger.getLogger(HandRoute.class);
	private int count=0;
	private String message;
	public HandRoute() {
		logger.info("create class: HandRoute - "+count);
	}
	
	public String checkInfo(String input) {
		count++;
		logger.info("===> input= "+input);
		logger.info("===> NumberReq= "+count);
		return input;
	}
	
	public String checkInfo(String input, int input2) {
		logger.info("******************* input= "+input);
		logger.info("===> input2= "+input2);
		return input;
	}
	
	public Object onStart(Object input) {
		logger.info("===> onStart= "+input);
		return input;
	}
	
	public BwacoInfo getSoDb(String input) {
		JsonObject json=new JsonObject();
		BwacoInfo bwaco=new BwacoInfo();
		bwaco.setSodb(json.getString("sodb","0191670"));
		logger.info("===> getSoDb= "+bwaco);
		return bwaco;
	}
	
	public Object onCheckBwaco(Object input) {
		try {
			logger.info("===> onCheckBwaco= "+input);
		} catch (Exception e) {
			logger.info("pase text loi roi",e);
		}
		
		return input;
	}
	
	public Object getReturnInfo(Object input) {
		try {
			logger.info("===> getReturnInfo= "+input);
			String[] req=input.toString().split("@@@");
			String node= req[0];
			String body=req[1];
			return loadNode(body,node);
		} catch (Exception e) {
			logger.info("pase text loi roi",e);
			return "errhandle";
		}
	}
	
	
	public Object getErr(Object input) {
		try {
			logger.info("===> request err= "+input);
			
		} catch (Exception e) {
			logger.info("pase text loi roi",e);
		}
		
		return input;
	}
	
	public Object testInput(Object input) {
		try {
			logger.info("===> testInput= "+input+" ************\nMessage= "+message);
			
		} catch (Exception e) {
			logger.info("pase text loi roi",e);
		}
		
		return input;
	}
	
	public Object testInput2(Object input,Object input2) {
		try {
			logger.info("===> testInput2= "+input+" ************\ninput2= "+input2);
			
		} catch (Exception e) {
			logger.info("pase text loi roi",e);
		}
		
		return input;
	}
	
	
	private static JsonObject loadNode(String input, String...tagName) throws Exception {
		
        try {
            JsonObject jsonRes = new JsonObject();
            if (Objects.isNull(input) || tagName.length < 1) {
                return jsonRes;
            }

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new ByteArrayInputStream(input.getBytes("UTF-8")));  
            
            NodeList node;
            for (String tag : Arrays.asList(tagName)) {
                node = doc.getElementsByTagName(tag);
                if (node != null && node.getLength() > 0) {
                    if (node.item(0).getChildNodes().getLength()>0) {
                        node = node.item(0).getChildNodes();
                        for (int i = 0; i < node.getLength(); i++) {
                            jsonRes.put(node.item(i).getNodeName(), node.item(i).getTextContent());
                        }
                    } else {
                        jsonRes.put(tag, node.item(0).getTextContent());
                    }
                }
            }
            return jsonRes;
        } catch (Exception e) {
            throw new Exception("Can't pase Tag to JsonObject!!! TagName= " + Arrays.asList(tagName).toString(), e);
        }
}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

	
}
