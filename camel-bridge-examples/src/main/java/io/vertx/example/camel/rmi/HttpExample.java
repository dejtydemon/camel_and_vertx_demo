package io.vertx.example.camel.rmi;

import io.vertx.camel.CamelBridge;
import io.vertx.camel.CamelBridgeOptions;
import io.vertx.camel.InboundMapping;
import io.vertx.camel.OutboundMapping;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;
import org.apache.camel.CamelContext;
import org.apache.camel.CamelContextAware;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static io.vertx.camel.OutboundMapping.fromVertx;

import java.io.FileInputStream;

/**
 * @author <a href="http://escoffier.me">Clement Escoffier</a>
 */
public class HttpExample extends AbstractVerticle {

	 public static void main(String[] args) {
		    Vertx vertx = Vertx.vertx();
		    DeploymentOptions option=new DeploymentOptions();
		    option.setWorker(true);
		    vertx.deployVerticle(HttpExample.class.getName(), option);
		  }


		  @Override
		  public void start() throws Exception {
			  CamelContext camel=new DefaultCamelContext();
			  camel.addRoutes(new RouteBuilder() {
				  @Override
				  public void configure() throws Exception {
					  from("direct:start").inputType(String.class).outputType(String.class)
					  
	                    .to("http://localhost:8080/aa");
				  }
				  
				  
				});
			  
				CamelBridge bridge = CamelBridge.create(vertx, new CamelBridgeOptions(camel)
					        .addOutboundMapping(OutboundMapping.fromVertx("camel-route").toCamel("direct:start"))  );

				camel.start();
				bridge.start();

		    
		    vertx.createHttpServer()
		        .requestHandler(this::invoke)
		        .listen(8088);

		  }

		  private void invoke(HttpServerRequest request) {
		    String param = request.getParam("name");
		    if (param == null) {
		      param = "vert.x";
		    }
		    vertx.eventBus().<String>send("camel-route", param, reply -> {
		    	System.out.println("==> response");
		      if (reply.failed()) {
		        request.response().setStatusCode(400).end(reply.cause().getMessage());
		      } else {
		        request.response().setStatusCode(0).end(reply.result().body());
		      }
		    });
		  }
}
