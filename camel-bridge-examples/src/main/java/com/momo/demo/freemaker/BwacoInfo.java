package com.momo.demo.freemaker;

public class BwacoInfo {
	private String sodb;
	private String thang;
	public String getSodb() {
		return sodb;
	}
	public void setSodb(String sodb) {
		this.sodb = sodb;
	}
	public String getThang() {
		return thang;
	}
	public void setThang(String thang) {
		this.thang = thang;
	}
	@Override
	public String toString() {
		return "BwacoInfo [sodb=" + sodb + ", thang=" + thang + "]";
	}
	
	
	
	
}
